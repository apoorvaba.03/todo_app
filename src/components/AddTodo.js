import { Button } from '@chakra-ui/button';
import { HStack, Input, useToast} from '@chakra-ui/react';
import React from 'react';
import {useState} from 'react';
import { nanoid } from 'nanoid';
function AddTodo({ addTodo })
{
    const toast = useToast()
    function handleSubmit(e)
    {
        e.preventDefault();
       // console.log(content);
        if(!content)
        {
            toast({
                title: 'No content',
                status:'error',
                duration:2000,
                isClosable:true,


            });
            return
        }

        const todo = {
            id:nanoid(),
            body:content,
        };
        //console.log(todo);
        addTodo(todo);
        setContent(' ')
    }

    const [content, setContent] = useState('');
    return <form onSubmit={handleSubmit}> 
    <HStack mt='8'>
        <Input variant="filled" placeholder="leraning chakra ui" value={ content }
         onChange={(e) => setContent(e.target.value) }/>
        <Button colorScheme="pink" px='8' type='submit'>AddTodo</Button>
    </HStack>

    </form>
}
export default AddTodo;